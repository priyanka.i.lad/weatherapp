$(document).ready(function() {
  let getPositionCoords = getPositions();
  let weekdays = getWeekdays();
  getPositionCoords.then(function(position) {
    if (position && position.coords) {
      let latlng = position.coords.latitude + "," + position.coords.longitude;
      getAddress(latlng);
      getWeather(latlng);
    } else {
      alert("Oops, Geolocation is not supported in your browser");
    }
  });

  function getAddress(latlng) {
    let url =
      "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
      latlng +
      "&key=AIzaSyCuugDMliUtuYZ1tT2PZbgB_LMvOYi0wFU";
    fetch(url)
      .then(function(response) {
        if (response.status !== 200) {
          console.log(
            `Something went wrong. Response status: ${response.status}`
          );
          return;
        }
        response.json().then(function(data) {
          if (data && data["plus_code"]["compound_code"]) {
            let address = data["plus_code"]["compound_code"];
            let addressArray = address.split(",");
            address = addressArray[0].split(" ")[1] + ", " + addressArray[1];
            $("#spnAddress").text(address);
          }
        });
      })
      .catch(function(error) {
        console.error();
      });
  }

  function getWeather(positions) {
    //this request has to be done as jsonp because of cross-domain policies on the browser
    //adding ?callback=? makes the server treat the request as jsonp, the other option would be
    //to use .ajax(dataType: "jsonp")
    $.getJSON(
      "https://api.forecast.io/forecast/b59cb056ae86ddcff4531258c647bf0d/" +
        positions +
        "?callback=?",
      function(wData) {
        console.log(wData);
        if (wData) {
          // $("#divMain").css("display", "block");
          let currentWeather = wData.currently;
          var skycon = new Skycons({ color: "black" });
          displayCurrentData(currentWeather, skycon);
          displayForecastData(wData.daily, skycon);
          skycon.play();
        }
      }
    );
  }

  function displayForecastData(daily, skycon) {
    let arrForecast = getForecCastData(daily);
    let forecastDivs = $("#forecastData div");

    let day = new Date().getDay();
    arrForecast.forEach(function(item, index) {
      displaySkycons("dayIcon" + (index + 1), skycon, item.icon);
      displayWeekday(day, index + 1);
      day = getNextWeekday(day);
      $("#spnTempDay" + (index + 1)).text(parseInt(item.temperatureHigh));
    });
  }

  function getWeekdays() {
    return [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ];
  }

  function displayWeekday(day, i) {
    let day_name = weekdays[day];
    $("#spnDay" + i).text(day_name);
  }

  function getNextWeekday(day) {
    if (day === 6) return 0;
    else return ++day;
  }

  function displayCurrentData(currentWeather, skycon) {
    $("#spnIconInfo").text(currentWeather.summary);
    $("#spnCurrentTemp").text(parseInt(currentWeather.temperature));

    $("#spnprecipProbability").text(
      currentWeather.precipProbability * 100 + "%"
    );
    $("#spnWind").text(parseInt(currentWeather.windSpeed) + " Mph");
    $("#spnFeelslike").text(parseInt(currentWeather.apparentTemperature));

    displaySkycons("currentWeatherIcon", skycon, currentWeather.icon);
    displaySkycons("precipProbabilityIcon", skycon, currentWeather.precipType);
    displaySkycons("windIcon", skycon, "WIND");
  }

  function displaySkycons(id, skycon, prop) {
    let icon = getIcon(prop, skycon);
    skycon.add(id, icon);
  }

  function getWeatherData(positions) {
    let header = {
      "Access-Control-Allow-Origin": "*"
    };
    let url =
      "https://api.forecast.io/forecast/b59cb056ae86ddcff4531258c647bf0d/" +
      //"https://api.darksky.net/forecast/575e286760448fa4918bc9786226d699/" +
      positions +
      "?callback=?";
    fetch(url, {
      headers: header
    })
      .then(function(response) {
        if (response.status !== 200) {
          console.log(
            `Something went wrong. Response status: ${response.status}`
          );
          return;
        }

        response.json().then(function(data) {
          console.log(data);
        });
      })
      .catch(function(err) {
        console.log("Fetch Error: " + err);
      });
  }

  function getPositions() {
    if (navigator.geolocation) {
      return new Promise((resolve, reject) =>
        navigator.geolocation.getCurrentPosition(resolve, reject)
      );
    } else {
      return new Promise(resolve => ({}));
    }
  }

  function getForecCastData(daily) {
    let arrForecast = [];
    if (daily) {
      for (let i = 1; i <= 5; i++) {
        arrForecast.push(daily.data[i]);
      }
    }
    return arrForecast;
  }

  function getIcon(data, skycon) {
    data = data.toString().toUpperCase();
    if (data === "CLEAR-DAY") {
      return Skycons.CLEAR_DAY;
    } else if (data === "CLEAR-NIGHT") {
      return Skycons.CLEAR_NIGHT;
    } else if (data === "PARTLY-CLOUDY-DAY") {
      return Skycons.PARTLY_CLOUDY_DAY;
    } else if (data === "PARTLY-CLOUDY-NIGHT") {
      return Skycons.PARTLY_CLOUDY_NIGHT;
    } else if (data === "CLOUDY") {
      return Skycons.CLOUDY;
    } else if (data === "RAIN") {
      return Skycons.RAIN;
    } else if (data === "SLEET") {
      return Skycons.SLEET;
    } else if (data === "SNOW") {
      return Skycons.SNOW;
    } else if (data === "WIND") {
      return Skycons.WIND;
    } else if (data === "FOG") {
      return Skycons.FOG;
    }
  }
});
